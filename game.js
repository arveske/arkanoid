/*
	start: function() {},
*/
var game = {
	ctx: undefined,
	sprites: {
		background: undefined
	},
	start() {
		var canvas = document.getElementById("mycanvas");
		this.ctx = canvas.getContext("2d");
		
		this.sprites.background = new Image();
		this.sprites.background.src = "images/background.png";
		this.run();
	},
	render() {
		this.ctx.drawImage(this.sprites.background, 0, 0);

	},
	run() {
		this.render();

		window.requestAnimationFrame(() => {
			game.run();
		});
	}
};

/*
	window.addEventListener("load", function() {
		game.start();
	})
*/
window.addEventListener("load", () => {
	game.start();
});
